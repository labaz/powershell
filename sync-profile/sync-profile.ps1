$sourceDirs = @"
$env:LOCALAPPDATA
$env:APPDATA
"@ -split [char]10 | ForEach-Object {$_ -replace [char]13, ''} | Where-Object {$_}

$subdirs = @"
Chromium
zoxide
lf
librewolf
Microsoft\Windows Terminal
Syncthing
qBittorrent
foobar2000-v2
vlc
obs-studio
synergy
"@ -split [char]10 | ForEach-Object {$_ -replace [char]13, ''} | Where-Object {$_}

$excludePaths = @"
**/storage/
Application/
**/*Cache/
"@ -split [char]10 | ForEach-Object {$_ -replace [char]13, ''} | Where-Object {$_}

$destinationPath = "$env:USERPROFILE\_AppData"

 ######    #######  
##    ##  ##     ## 
##        ##     ## 
##   #### ##     ## 
##    ##  ##     ## 
##    ##  ##     ## 
 ######    #######

$excludePathString = ($excludePaths | ForEach-Object { "--exclude '$_'" }) -join ' '
$destinationPath = $destinationPath -replace '\\', '/'

# Write-Host $excludePathString

# create destination
New-Item -Path $destinationPath -ItemType Directory -ErrorAction SilentlyContinue | Out-Null

# loop over paths to search, loop over paths to add. if found, copy to target
$foundDirs = @()
foreach($sourceDir in $sourceDirs) {
  Set-Location $sourceDir
  foreach($subdir in $subdirs) {
    $dirToFind = Join-Path $sourceDir $subdir
    If(Test-Path -LiteralPath $dirToFind) {
      # Write-Host $dirToFind -ForegroundColor Green
      $relativePath = Get-Item $dirToFind | Resolve-Path -Relative -RelativeBasePath $sourceDir
      $sourceDir, $relativePath, $dirToFind = @($sourceDir, $relativePath, $dirToFind) | ForEach-Object {$_ -replace '\\', '/'}
      $foundDirs += , @($sourceDir, $relativePath, $dirToFind)
    }
  }
}

ForEach($dirObject in $foundDirs) {
  # cut off the piece to 
  # $dirObject
  $parentFullPath = $dirObject[0]
  $parentName = Split-Path $parentFullPath -Leaf

  $sourceRelativePath = $dirObject[1] -replace '^\./', ''

  $sourceFullPath = $dirObject[2]

  $destinationFullPath = Join-Path $destinationPath $parentName $sourceRelativePath
  $destinationFullPath = $destinationFullPath -replace '\\', '/'

  Write-Host $sourceFullPath -NoNewLine
  Write-Host " > " -NoNewline
  Write-Host $parentName -ForegroundColor Green -NoNewline
  Write-Host "/" -NoNewline
  Write-Host $sourceRelativePath -ForegroundColor Magenta

  # Write-Host $destinationFullPath

  $stopwatch = [Diagnostics.Stopwatch]::StartNew()

  # SYNC
  Invoke-Expression "rclone sync `"$sourceFullPath`" `"$destinationFullPath`" $excludePathString --ignore-errors"

  Write-Host "Elapsed: " -ForegroundColor Gray -NoNewline
  Write-Host $stopwatch.Elapsed -ForegroundColor Magenta
  
}
