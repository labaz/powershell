<#
    Create profiling measures for a table
    - count rows
    - which column may be key
    Per column:
    - count nulls
    - distinct count of values
    - top X values concatenated

    Process:
    * PBI desktop must be running with the needed table
    - locate SourceTable
    - create empty calculated table SourceTable_Profiling. 
        (it will contain measures)
    - get columns of SourceTable
        - loop over columns and create measures
#>

# to give good German characters in clipboard
[Console]::OutputEncoding = [System.Text.Encoding]::UTF8
$OutputEncoding = [System.Text.Encoding]::UTF8

. .\psPbiDesktop.ps1

Clear-Host

$pbi = Get-LocalPBIDesktopInstances
If(-not $pbi) {return}

$port = $pbi.LocalPort
$SSASTabularHostPort = "localhost:$port"

$db = Get-SSASTabularDBs $SSASTabularHostPort

$profilingSuffix = '_Profiling'

# we take only user data tables
$tables = Get-SSASTabularTables $db | `
    Where-Object { 
        # not internal PBI tables
        $_.IsPrivate -eq $false -and 
        # not generated date tables
        $_.Annotations.Name -notcontains '__PBI_LocalDateTable' -and
        # exclude the tables this script creates
        $_.Name -notlike "*$profilingSuffix" -and
        # exclude calculated tables
        $_.Partitions.SourceType -ne 'Calculated'
    }



ForEach($sourceTable in $tables) {
    $sourceTableName = $sourceTable.Name
    Write-Host $sourceTableName -ForegroundColor Green
    $columnNamePatternToExclude = '^\$'
    $sourceTableColumns = $sourceTable.Columns | `
        Where-Object {
            $_.Name -notmatch $columnNamePatternToExclude -and
            $_.Type -ne 'RowNumber'
        }

    # create empty table for profiling
    $targetTableDAXExpression = '{("1")}'
    $targetTableName = "$($sourceTableName)$profilingSuffix"

    # remove the table
    $targetTableNameRegex = $targetTableName -replace '([\[\]\\\(\)])', '\$1'
    Remove-SSASTables $db $targetTableNameRegex

    $targetTableDefinition = @{
        Name = $targetTableName
        IsHidden = $false
        Expression = $targetTableDAXExpression
    }

    $tableDefinitionList = @()
    $tableDefinitionList += $targetTableDefinition

    New-SSASTables $db $tableDefinitionList
    
    $measureDefinitionList = @()
    
    # general table-based measure
    
    # TODO: PARK this because it mixes with the first measure bucket
    $measureDefinitionList += @(
        # @{
        #     KPIName = "Row count"
        #     SourceColumnName = "Row count"
        #     Name = "_ $sourceTableName ROWCOUNT"
        #     Expression = "COUNTROWS('$sourceTableName')"
        #     FormatString = "#,0"
        # }
    )
    
    $columnNumber = 1
    $columnList = @()
    
    $measureDefinitionList | ForEach-Object {
        $columnList += ,@($columnNumber, $_.KPIName)
        $columnNumber++
    }

    ForEach($sourceColumn in $sourceTableColumns) {

        $sourceColumnName = $sourceColumn.Name
        $sourceColumnNameQualified = "'$sourceTableName'[$sourceColumnName]"
        # Write-Host $sourceColumnName -ForegroundColor Cyan
        
        # these will be columns in DATATABLE
        $kpiDatatableDefinition = @(
            @{Name = "Sort"; Datatype = "INTEGER"},
            @{Name = "KPI"; Datatype = "STRING"}
        )

        $measureDefinitionListForColumn = @(
            @{
                KPIName = 'Distinct values'
                SourceColumnName = $sourceColumnName
                Name = "$sourceColumnName - Distinct"
                Expression = "DISTINCTCOUNT($sourceColumnNameQualified)"
                FormatString = "#,0"
            },
            @{
                KPIName = 'NULL'
                SourceColumnName = $sourceColumnName
                Name = "$sourceColumnName - NULL"
                Expression = "CALCULATE(COUNTROWS('$sourceTableName'), FILTER('$sourceTableName', $sourceColumnNameQualified = BLANK()))"
                FormatString = "#,0"
            },
            @{
                KPIName = "EMPTY"
                SourceColumnName = $sourceColumnName
                Name = "$sourceColumnName - EMPTY"
                Expression = "CALCULATE(COUNTROWS('$sourceTableName'), FILTER('$sourceTableName', FORMAT($sourceColumnNameQualified, `"`") = `"`"))"
                FormatString = "#,0"
            },
            @{
                KPIName = "Top"
                SourceColumnName = $sourceColumnName
                Name = "$sourceColumnName - Top"
                Expression = @"

VAR NforTopN = 5 
VAR RankedValues = 
    ADDCOLUMNS(
        VALUES($sourceColumnNameQualified),
        "RowCount", CALCULATE(COUNTROWS($($sourceTableName))),
        "Rank", RANKX(ALL($($sourceColumnNameQualified)), CALCULATE(COUNTROWS('$sourceTableName')), , DESC, Dense)
    )
VAR FilteredValues = 
    FILTER(
        RankedValues, 
        [Rank] <= NforTopN
    )
VAR MaxRowCountAtN = 
    MAXX(TOPN(TopN, FilteredValues, [RowCount], DESC), [RowCount])
VAR CountAtMaxRowCount = 
    COUNTROWS(FILTER(FilteredValues, [RowCount] = MaxRowCountAtN))
VAR FinalValues = 
    IF(
        CountAtMaxRowCount > NforTopN,
        TOPN(NforTopN, FILTER(FilteredValues, [RowCount] = MaxRowCountAtN), $sourceColumnNameQualified, ASC),
        FilteredValues
    )
RETURN
    CONCATENATEX(
        FinalValues, 
        $sourceColumnNameQualified, 
        ", ",
        [RowCount], 
        DESC
    )
"@
                FormatString = ""
            }

        )
        
        $measureDefinitionList += $measureDefinitionListForColumn
        $columnList += ,@($columnNumber, $sourceColumnName)

        $columnNumber++
    }
    
    # create rows for DATATABLE
    $rowStrings = @()
    ForEach($rowDefinition in $columnList) {
        # 1, ROWCOUNT
        # 2, KPIx
        $kpiColumnIndex = 0
        $rowValues = @()
        ForEach($rowValue in $rowDefinition) {
            # Name, Datatype
            # Name, Datatype
            $columnName = $kpiDatatableDefinition[$kpiColumnIndex].Name
            $columnDatatype = $kpiDatatableDefinition[$kpiColumnIndex].Datatype

            $columnValue = $rowValue
            If($columnDatatype -eq "STRING") {
                $columnValue = "`"$columnValue`""
            }
            
            $rowValues += $columnValue
            $kpiColumnIndex++
        }
        $rowString = $rowValues -join ', '
        $rowString = "{$rowString}"
        # Write-Host $rowString -ForegroundColor Magenta
        $rowStrings += $rowString
    }

    $DAXDatatableRows = $rowStrings -join ",`n"

    $datatableColumns = @()
    ForEach($columnDefinition in $kpiDatatableDefinition) {
        $columnName = $columnDefinition.Name
        $columnDatatype = $columnDefinition.Datatype
        $datatableColumnString = "`"$columnName`", $columnDatatype"
        $datatableColumns += $datatableColumnString
    }

    $DAXDatatableColumns = $datatableColumns -join ",`n"


    New-SSASMeasures $db $targetTableName $measureDefinitionList $true
    
    $DAXDatatableExpression = @"
    DATATABLE(
        $DAXDatatableColumns,
        {
            $DAXDatatableRows    
        }
    )
"@

    # Write-Host $DAXDatatableExpression -ForegroundColor Magenta
    $DAXDatatableExpression | clip

    $kpiTableName = "$($targetTableName)_KPIs"
    $kpiTableDefinition = @{
        Name = $kpiTableName
        IsHidden = $false
        Expression = $DAXDatatableExpression
    }
    $tableDefinitionList = @()
    $tableDefinitionList += $kpiTableDefinition
    New-SSASTables $db $tableDefinitionList

    # and now, create conditional KPIs, as many as there are rows in the KPI table
    # Dynamic measure 1 = IF(HASONEVALUE('Table'[KPI]), SWITCH(VALUES('Table'[KPI]), {"RowCount", 1}, BLANK()), BLANK())
     # the part {} should be "KPI row value", [Measure name]
    $formulaSwitchConditions = @() # "KPI row value", [Measure name]
    $measureBuckets = @{} # if there is >1 measure per value, put them into buckets
    ForEach($switchDataRow in $columnList) {
        # find respective KPIs in $measureDefinitionList
        $switchValue = $switchDataRow[1]
        $foundMeasures = @(
            $measureDefinitionList | `
                Where-Object {
                    $_.SourceColumnName -eq $switchValue
                }
        )

        # Write-Host "$switchValue " -ForegroundColor Magenta -NoNewline
        # Write-Host $foundMeasures.Count -ForegroundColor Green

        $measureIndex = 1
        ForEach($foundMeasure in $foundMeasures) {
            $switchTuple = "`"$switchValue`", [$($foundMeasure.Name)]"
            
            If(-not ($measureIndex -in $measureBuckets.Keys)) {
                $measureBuckets[$measureIndex] = @()
            }

            $measureBuckets[$measureIndex] += $switchTuple

            $measureIndex++
        }
    }

    $dynamicMeasurePrefix = "Dynamic Measure "
    
    $dynamicMeasureDefinitions = @()

    ForEach($bucketNumber in $measureBuckets.Keys) {
        $switchString = $measureBuckets[$bucketNumber] -join ",`n"

        $dynamicMeasureExpression = @"
IF(
    HASONEVALUE('$kpiTableName'[KPI]), 
    SWITCH(
        VALUES('$kpiTableName'[KPI]), 
        $switchString,
        BLANK()),
    BLANK()
)
"@
        Write-Host $dynamicMeasureExpression -ForegroundColor Magenta
        $dynamicMeasureDefinition = @{
            Name = "$dynamicMeasurePrefix $bucketNumber"
            Expression = $dynamicMeasureExpression
            FormatString = "#,0"
        }

        $dynamicMeasureDefinitions += $dynamicMeasureDefinition
    }

    New-SSASMeasures $db $kpiTableName $dynamicMeasureDefinitions $true
}
