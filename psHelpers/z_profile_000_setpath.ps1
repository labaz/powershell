$pathsToAdd = @"
$env:USERPROFILE\totalcmd\
$env:USERPROFILE\totalcmd\programs\
$env:USERPROFILE\totalcmd\programs\gzip
$env:USERPROFILE\totalcmd\programs\nvim\bin
$env:USERPROFILE\totalcmd\programs\az\SourceDir\Microsoft SDKs\Azure\CLI2\wbin
$env:USERPROFILE\totalcmd\programs\sysinternals
$env:USERPROFILE\totalcmd\programs\xpdf
$env:USERPROFILE\totalcmd\programs\syncthing
$env:USERPROFILE\totalcmd\programs\zig
$env:USERPROFILE\totalcmd\programs\linux
$env:USERPROFILE\totalcmd\programs\figlet
$env:USERPROFILE\totalcmd\programs\ffmpeg\bin
$env:USERPROFILE\totalcmd\programs\zoxide
$env:USERPROFILE\totalcmd\programs\git\bin
$env:USERPROFILE\totalcmd\programs\oh-my-posh\bin
$env:USERPROFILE\totalcmd\programs\winget
$env:USERPROFILE\totalcmd\programs\pwsh
$env:USERPROFILE\totalcmd\programs\nodejs
$env:USERPROFILE\totalcmd\programs\winget
$env:USERPROFILE\totalcmd\programs\ImageMagick
"@ -split [char]10 | ForEach-Object {$_ -replace [char]13, ''} | Where-Object {$_}

# Write-Host "Paths to add"
# Write-Host $pathsToAdd
# Write-Host $pathsToAdd.Count

$envVarName = 'PATH'

$separator = ';'

$paths = $env:PATH -split $separator | Where-Object {$_}

# Write-Host "$envVarName"
# Write-Host $paths
# Write-Host $paths.Count

$newlyFoundPaths = @()

ForEach($pathToAdd in $pathsToAdd) {
    
    if (-not (Test-Path "$pathToAdd") ) { 
      #Write-Host "Path does not exist $pathToAdd" -ForegroundColor Red
      continue
    }

    # check if path in path

    If ($pathToAdd -in $paths) {
      #Write-Host "Skipping $pathToAdd because it's already added" -ForegroundColor Gray
      continue
    }

    # Write-Host "$pathToAdd"
    $newlyFoundPaths += $pathToAdd
}

If($newlyFoundPaths.Count -eq 0) {
    # Write-Host "No new paths found, all are already in $envVarName" -ForegroundColor DarkGreen
    return
}

$pathsToSet = $paths + $newlyFoundPaths

$pathString = $pathsToSet -join $separator

# Write-Host $pathString -ForegroundColor Magenta

$envVarValue = $pathString

# Write-Host "Setting $envVarName" -ForegroundColor Cyan
# Write-Host $pathString -ForegroundColor Magenta

[System.Environment]::SetEnvironmentVariable($envVarName, $envVarValue, [System.EnvironmentVariableTarget]::User)
