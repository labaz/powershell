<#

Function adb-push {
    param(
        $targetSubdirOnDevice,
        $sourcesToPush,
        [switch]$pushToSD = $true
    )

    $targetSubdirOnDevice = $targetSubdirOnDevice -replace '\\' '/'

    # if source is a single string, make an array
    $sourcesToPush_Array = @()
    $sourcesToPush_Array += ,$sourcesToPush

    $sourcesToPush = $sourcesToPush_Array

    # check if device is connected
    $adbDevices = adb devices | Select-String '\bdevice\b'
    If(-not $adbDevices) {
        Write-Host "adb device not found" -ForegroundColor Red
        return
    }

    #####################

    # by default push to internal memory
    $storageEmulated0 = "/storage/emulated/0"
    $targetOnDevice = "$storageEmulated0/$targetSubdirOnDevice"
    
    # if SD card found, push there
    If($pushToSD) {
        $storage = adb shell ls /storage
        $sdcard = $storage | Where-Object {[regex]::Match($_,'[A-Z0-9]{4}-[A-Z0-9]{4}').Success}
        If($sdcard) {
            $targetOnDevice = "/storage/$sdcard/$targetSubdirOnDevice"
        } 
    }
    
    Write-Host "Target on device: " -NoNewline -ForegroundColor Gray
    Write-Host $targetOnDevice -ForegroundColor Magenta -NoNewline
    
    # check if target exists and create
    $targetExists = -not (adb shell ls "$targetOnDevice" 2>&1 | Select-String "No such file or directory")
    If(-not $targetExists) {
        adb shell mkdir "$targetOnDevice"
        Write-Host " CREATED"
    } else {
        Write-Host ""
    }
    
    # loop and push
    $timerTotal = [Diagnostics.Stopwatch]::StartNew()
    ForEach($source in $sourcesToPush) {
        Write-Host $source -ForegroundColor Cyan
        $timer = [Diagnostics.Stopwatch]::StartNew()
    
        adb push --sync "$source" "$targetOnDevice"
        
        Write-Host "Elapsed: " -ForegroundColor Gray -NoNewline
        Write-Host $timer.Elapsed.ToString() -ForegroundColor Green
    }
    
    Write-Host "Elapsed total: " -ForegroundColor Gray -NoNewline
    Write-Host $timerTotal.Elapsed.ToString() -ForegroundColor Green
    
}

# adb-push Movies The.Mummy.1999.Extended.Edition.BDRip.1080p.by.Martokc.mkv
#>

Function adb-push {
    param(
        $targetSubdirOnDevice,
        [Parameter(ValueFromPipeline)]$sourceToPush,
        [switch]$pushToSD = $true
    )

    begin {
        $targetSubdirOnDevice = $targetSubdirOnDevice -replace '\\', '/'
    
        # check if device is connected
        $adbDevices = adb devices | Select-String '\bdevice\b'
        If(-not $adbDevices) {
            Write-Host "adb device not found" -ForegroundColor Red
            retu`rn
        }
    
        #####################
    
        # by default push to internal memory
        $storageEmulated0 = "/storage/emulated/0"
        $targetOnDevice = "$storageEmulated0/$targetSubdirOnDevice"
        
        # if SD card found, push there
        If($pushToSD) {
            $storage = adb shell ls /storage
            $sdcard = $storage | Where-Object {[regex]::Match($_,'[A-Z0-9]{4}-[A-Z0-9]{4}').Success}
            If($sdcard) {
                $targetOnDevice = "/storage/$sdcard/$targetSubdirOnDevice"
            } 
        }
        
        Write-Host "Target on device: " -NoNewline -ForegroundColor Gray
        Write-Host $targetOnDevice -ForegroundColor Magenta -NoNewline
        
        # check if target exists and create
        $targetExists = -not (adb shell ls "$targetOnDevice" 2>&1 | Select-String "No such file or directory")
        If(-not $targetExists) {
            adb shell mkdir "$targetOnDevice"
            Write-Host " CREATED"
        } else {
            Write-Host ""
        }
        
        $timerTotal = [Diagnostics.Stopwatch]::StartNew()
    } # begin 

    process {
        # loop and push
        Write-Host $source -ForegroundColor Cyan
        $timer = [Diagnostics.Stopwatch]::StartNew()
    
        adb push --sync "$sourceToPush" "$targetOnDevice"
        
        Write-Host "Elapsed: " -ForegroundColor Gray -NoNewline
        Write-Host $timer.Elapsed.ToString() -ForegroundColor Green
    }
    
    end {
        Write-Host "Elapsed total: " -ForegroundColor Gray -NoNewline
        Write-Host $timerTotal.Elapsed.ToString() -ForegroundColor Green
    }
    
}
# adb-push Movies The.Mummy.1999.Extended.Edition.BDRip.1080p.by.Martokc.mkv
