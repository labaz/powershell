These helpers help to do small daily things.

To make use of the helpers, download them into a single directory and add this directory to PS profile.

The PS profile is a PS file that loads with every load of pwsh. Its path is in PS variable `$PROFILE`. It is located in `"$env:userprofile\Documents\PowerShell\Microsoft.PowerShell_profile.ps1"`. 

Edit the profile as follows:

### `$PROFILE`

```powershell
Get-ChildItem "<...>/psHelpers/*.ps*" | ForEach-Object {. $_}
```
