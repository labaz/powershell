Function cpath {
    $arguments = $args -join ' '
    $currentPath = (pwd).Path
    $currentPath | clip
}
