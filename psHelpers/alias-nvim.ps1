# git aliases in powershell
# like in zsh https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/plugins/git/git.plugin.zsh
# but aliases are different due to hardcoded ps aliases and personal preference

Function n {
    $arguments = $args -join ' '
    Invoke-Expression "nvim '$arguments'"
}
