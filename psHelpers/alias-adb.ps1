<#
    Create aliases for adb
    adb connect
    adb disconnect
    adb push
    adb pull
    adb devices
    ...
#>

Function adc {
    # connects to devices on local network
    param(
        [string]$i
    )

    if ($i -match '^.*:.*$') {
        $cmd = "adb connect 192.168.0.$i"
        Invoke-Expression $cmd
    }
}

Function ad {
    Invoke-Expression 'adb devices'
}

Function add {
    param(
        [string]$i
    )
    Invoke-Expression "adb disconnect $i"
}

Function ap {
    # dependency on adb-push
    param(
        [string]$f1,
        [string]$f2
    )
    # adb-push Target SourcePaths PushToSD(switch on by default)
    Invoke-Expression "adb-push `"$f1`" `"$f2`""
}

Function apl {
    param(
        [string]$f1,
        [string]$f2
    )
    Invoke-Expression "adb pull `"$f1`" `"$f2`""
}

Function alp {
    Invoke-Expression "adb shell pm list packages" | ForEach-Object {[regex]::Replace($_,'^.*:','')} | Sort-Object
}

Function au {
    param(
        [string]$i
    )
    Invoke-Expression "adb shell pm uninstall --user 0 $i"
}

Function aie {
    param(
        [string]$i
    )
    Invoke-Expression "adb shell pm install-existing $i"
}

Function ai {
    param(
        [string]$i
    )
    Invoke-Expression "adb install `"$i`""
}

Function adu {
    param(
        [string]$i
    )
    Invoke-Expression "adb shell pm disable-user --user 0 `"$i`""
}

Function ae {
    param(
        [string]$i
    )
    Invoke-Expression "adb shell pm enable `"$i`""
}

