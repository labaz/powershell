Function Find-Process {
	param(
		$Id,
		$CommandLine
	)

	# parse CommandLine and escape ', \, " with \
	$cleanCommandLine = $CommandLine -replace '(?<!\\)(["\\''])', '\$1'

	$filterString = ""
	If($Id) {
		$filterString = "ProcessId = $Id"
	}
	ElseIf ($CommandLine) {
		$filterString = "CommandLine = `"$cleanCommandLine`""
	}
	else {
		Write-Warning "No PID or CommandLine provided"
		return
	}
	Write-Host $filterString -ForegroundColor Magenta
	try {
		Get-CimInstance Win32_Process -Filter $filterString
	}
	catch {
		Write-Warning "No process found"
		return
	}
}
<#
Test:
$processId = 16164

$process = Find-Process -Id $processId

Find-Process -CommandLine $process.CommandLine | Select-Object * | Format-List 
#>
