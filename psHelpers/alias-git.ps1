# git aliases in powershell
# like in zsh https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/plugins/git/git.plugin.zsh
# but aliases are different due to hardcoded ps aliases and personal preference
Function glazy {
  $message = $args[0]
  $currentDir = (Get-Item .).Name
  $currentTimestamp = Get-Date
  $currentTimestampString = $currentTimestamp.ToString('yyyy-MM-dd HH:mm:ss')
  $defaultMessage = "$currentDir $currentTimestampString"

  If(-not $message) {
    $message = $defaultMessage
  }

  Invoke-Expression "git add ."
  Invoke-Expression "git commit -m '$message'"
  Invoke-Expression "git push"
}

Function gcmsg {
    $arguments = $args -join ' '
    Invoke-Expression "git commit --message '$arguments'"
}

Function gcam {
    $arguments = $args -join ' '
    Invoke-Expression "git commit --all --message '$arguments'"
}

Function gpl {
    Invoke-Expression "git pull"
}

Function gpsh {
    Invoke-Expression "git push"
}

Function gst {
    $arguments = $args -join ' '

    Invoke-Expression "git status $arguments"
}

Function gd {
    $arguments = $args -join ' '
    Invoke-Expression "git diff $arguments"
}

Function grst {
    $arguments = $args -join ' '
    Invoke-Expression "git restore --staged $arguments"
}

Function ga {
    $arguments = $args -join ' '
    Invoke-Expression "git add $arguments"
}

Function grm {
    $arguments = $args -join ' '
    Invoke-Expression "git rm $arguments"
}

Function gmv {
    $arguments = $args -join ' '
    Invoke-Expression "git mv $arguments"
}
