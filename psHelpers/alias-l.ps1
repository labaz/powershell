Function l {
  $argsString = $args -join ' '
  switch ($true) {
    {$argsString -eq '-tr'} {
      Get-ChildItem | Sort-Object LastWriteTime -Descending
    }
    {$argsString -eq '-t'} {
      Get-ChildItem | Sort-Object LastWriteTime
    }
    default {
      Get-ChildItem
    }
  }
}
