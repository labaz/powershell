# git aliases in powershell
# like in zsh https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/plugins/git/git.plugin.zsh
# but aliases are different due to hardcoded ps aliases and personal preference

$pumlJarFullPath = "$env:USERPROFILE\totalcmd\programs\plantuml\plantuml-1.2024.6.jar"

Function psvg {
    $arguments = $args -join ' '
    Invoke-Expression "set PLANTUML_LIMIT_SIZE=16384 && java -jar '$pumlJarFullPath' -tsvg '$arguments'"
}

Function ppdf {
    $arguments = $args -join ' '
    Invoke-Expression "set PLANTUML_LIMIT_SIZE=16384 && java -jar '$pumlJarFullPath' -tpdf '$arguments'"
}
