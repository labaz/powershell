function Stop-ProcessTree {
    param (
        [Parameter(Mandatory=$true)]
        [int]$ParentPid
    )

    # Get the child processes of the specified parent process
    $childPids = Get-CimInstance -ClassName Win32_Process -Filter "ParentProcessId = $ParentPid" | Select-Object -ExpandProperty ProcessId

    # Recursively stop each child process tree
    foreach ($childPid in $childPids) {
        Stop-ProcessTree -ParentPid $childPid
    }

    # Stop the parent process
    try {
        $process = Get-Process -Id $ParentPid -ErrorAction Stop
        Write-Host "Found process $($process.Name) PID " -NoNewline
        Write-Host $process.Id -ForegroundColor Cyan
        Write-Host $process.CommandLine -ForegroundColor Magenta
        $process | Stop-Process -Force
        Write-Output "Stopped process PID: $ParentPid"
    } catch {
        Write-Warning "Process with PID $ParentPid not found."
    }
}
