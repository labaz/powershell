function Add-Path {
    param(
        $pathToAdd,
        [switch]$System
    )
    
    $pathsSeparator = ';'
    $envVarName = 'PATH'

    # user or system
    $target = if ($System) { 
        [System.EnvironmentVariableTarget]::Machine 
    } else { 
        [System.EnvironmentVariableTarget]::User 
    }

    # clean up the trailing slash?
    $pathToAdd = $pathToAdd.TrimEnd('\\')

    # check if path exists
    if ( -not (Test-Path $pathToAdd) ) { 
        Write-Host "Path not found: $pathToAdd"
        return
    }

    # check if path in path
    $paths = ([Environment]::GetEnvironmentVariable($envVarName, $target) -split $pathsSeparator) |`
        ForEach-Object { $_.TrimEnd('\\') }

    If ($pathToAdd -in $paths) {
        Write-Host "Path '$pathToAdd' already in $envVarName"
        return
    }

    $paths += $pathToAdd
    $pathString = $paths -join $pathsSeparator

    # add 
    $envVarValue = $pathString
    
    Write-Host "Adding '$pathToAdd' to $envVarName"
    # Write-Host $pathString

    [System.Environment]::SetEnvironmentVariable($envVarName, $envVarValue, $target)

}

# Add-Path 'C:\Program Files\Graphviz\bin'
