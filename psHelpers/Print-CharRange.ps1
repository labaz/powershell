Function Print-CharRange {
    param(
        [Parameter(Mandatory=$true)]
        [string[]]$ranges,
        
        [switch]$SingleLine
    )

    foreach ($range in $ranges) {
        # Check if the input is a range or a single value
        if ($range -match '\.\.') {
            # It's a range
            if ($range -match '0x') {
                $start, $end = $range -split '\.\.' | ForEach-Object { [convert]::ToInt32($_, 16) }
            } else {
                $start, $end = $range -split '\.\.' | ForEach-Object { [int]$_ }
            }
        } else {
            # Single value
            if ($range -match '0x') {
                $start = $end = [convert]::ToInt32($range, 16)
            } else {
                $start = $end = [int]$range
            }
        }

        # Iterate through the range and print each character
        $start..$end | ForEach-Object {
            Write-Host ('{0:X4}' -f $_) -NoNewline -ForegroundColor Magenta
            Write-Host (' {0}' -f $_) -NoNewline -ForegroundColor Cyan

            # Check if -SingleLine is active
            if ($SingleLine) {
                Write-Host (" $([char]$_) ") -NoNewline -ForegroundColor Green
            } else {
                Write-Host (" $([char]$_) ") -ForegroundColor Green
            }
        }

        # If -SingleLine is active, add a newline after processing each range
        if ($SingleLine) {
            Write-Host ""
        }
    }
}

