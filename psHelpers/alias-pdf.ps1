# depends on pdfseparate
# http://poppler.freedesktop.org

Function pdfsep {
  $arguments = $args -join ' '
  $item = Get-Item $arguments

  $sourceBasename = $item.BaseName
  $sourceDir = $item.DirectoryName
  $sourcePath = $item.FullName

  $expression = "pdfseparate '$sourcePath' '$($sourceBasename)_%05d.pdf'"
  Write-Host $expression

  Invoke-Expression $expression
}
