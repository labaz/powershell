<#
    This creates alias 'history' that works like in bash
    To keep history long, we set history length to 10000
#>
Set-PSReadlineOption -MaximumHistoryCount 10000

Function wrapper_history {Get-Content -Path (Get-PSReadlineOption).HistorySavePath}
Set-Alias -Name history -Value wrapper_history -ErrorAction SilentlyContinue
