$appPath = "$env:USERPROFILE\totalcmd\programs\IrfanView\i_view64.exe"

Function ifw {
    $arguments = $args -join ' '
    $currentPath = (pwd).Path
    Invoke-Expression "$appPath $currentPath $arguments"
}
