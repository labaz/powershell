<#
    This creates an alias 'f' for figlet and sets the default font directory
    in this example, it looks for 'figlet' on PATH, but it's possible to hardcode it in $commandPath
    
    Requirements: 
    - figlet must be on PATH (to make a symlink to figlet, look at ../make-symlinks/make-symlinks.ps1)
    - inside figlet dir, must be subdir 'fonts' with figlet fonts (get here: https://github.com/xero/figlet-fonts.git)

    Use:
> f -f 3d Run
 ███████
░██░░░░██
░██   ░██  ██   ██ ███████
░███████  ░██  ░██░░██░░░██
░██░░░██  ░██  ░██ ░██  ░██
░██  ░░██ ░██  ░██ ░██  ░██
░██   ░░██░░██████ ███  ░██
░░     ░░  ░░░░░░ ░░░   ░░

> f Test | clip
### now the text is in clipboard
#>


# all is wrapped into a function in order to hide local vars from session
Function f {
    
    # figlet exists on path?
    $commandName = 'figlet'
    
    $command = Get-Command $commandName -ErrorAction SilentlyContinue
    if (-not $command) {return}
    
    $commandPath = $command.Path
    $commandItem = Get-Item $commandPath -ErrorAction SilentlyContinue
    if (-not $commandItem) {return}
    
    $resolvedCommandPath = $commandItem.ResolvedTarget
    if (-not $resolvedCommandPath) {return}
    
    $commandDir = Split-Path $resolvedCommandPath -Parent

    # by default, set max width to current terminal width
    # if it's not specified, some texts don't display
    # can be overridden when -w is specified in the function call
    $width = $Host.UI.RawUI.WindowSize.Width

    $params = (
        '-d',    
        "$commandDir\fonts",
        '-w',
        $width
    ) -join ' '
    
    $arguments = $args -join ' '
    
    $fullCommand = "$resolvedCommandPath $params $arguments"
    Invoke-Expression $fullCommand
}
