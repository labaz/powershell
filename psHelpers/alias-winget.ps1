<#
    powershell aliases for winget
#>
Function wi {
    param(
        [string]$f1
    )
    Invoke-Expression "winget install $f1"
}

Function wf {
    param(
        [string]$f1
    )
    Invoke-Expression "winget find $f1"
}

Function wpk {
    Invoke-Expression "cd $env:LOCALAPPDATA\Microsoft\WinGet\Packages"
}

Function wln {
    Invoke-Expression "cd $env:LOCALAPPDATA\Microsoft\WinGet\Links"
}

Function wlu {
    Invoke-Expression "winget list --upgrade-available --include-unknown"
}

Function wun {
    param(
        [string]$f1
    )
    Invoke-Expression "winget uninstall $f1"
}

Function wu {
    param(
        [string]$f1
    )
    Invoke-Expression "winget upgrade $f1 --include-unknown --accept-package-agreements --accept-source-agreements"
}

Function wua {
    Invoke-Expression "winget upgrade --all --accept-package-agreements --accept-source-agreements"
}

Function wi {
    param(
        [string]$f1
    )

    Invoke-Expression "winget install $f1 --accept-package-agreements --accept-source-agreements"
}

