Function fcd {
  fzf |`
  ForEach-Object {
    $item = Get-Item "$_"; 
    $dir = $item.DirectoryName; 
    Set-Location $dir
  }
}

Function fn {
  fzf |`
  ForEach-Object {
    $item = Get-Item "$_"; 
    $filePath = $item.FullName; 
    n $filePath
  }
}
